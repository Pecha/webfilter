# <img src="docs/icon32.png" /> WebFilter
<a href="#instalace"><img src="https://shields.io/badge/-Rozšíření%20prohlížeče-565656" /></a>
<a href="https://jan.pecha.it/" target="_blank"><img src="https://shields.io/badge/Autor-Jan%20Pecha-157fbd" /></a>
<a href="https://gitlab.com/Pecha/webfilter/-/issues" target="_blank"><img src="https://shields.io/badge/GitLab-Issues-52c228" /></a>
<a href="https://www.buymeacoffee.com/pchcze" target="_blank"><img src="https://img.shields.io/badge/-☕%20Kupte%20mi%20a%20kávu-fbdb04" /></a>

**WebFilter umožňuje blokovat specifické webové stránky na základě kategorického blacklistu URL adres.**

_**Cíl: Zvyšovat produktivitu, chránit před rozptylováním a bránit prokrastinaci.**_

Jazyk zobrazení stránky:

<a href="./README.md"><img src="https://raw.githubusercontent.com/madebybowtie/FlagKit/master/Assets/PNG/GB.png" /> English</a>
/ <a href="./README.CS.md"><img src="https://raw.githubusercontent.com/madebybowtie/FlagKit/master/Assets/PNG/CZ.png" /> **Česky**</a>

* [Instalace](#instalace)
* [Funkce](#funkce)
    * [Filtrování webových stránek podle kategorií](#filtrování-webových-stránek-podle-kategorií)
    * [Volitelný časový zámek](#volitelný-časový-zámek)
* [Licence](#license)
* [Podpora](#podpora)
* [Líbí se vám WebFilter?](#líbí-se-vám-webfilter)

## Instalace
| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/opera/opera_48x48.png" alt="Opera" width="48px" height="48px" /><br/>Opera](https://addons.opera.com/cs/extensions/details/webfilter/) | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="48px" height="48px" /><br/>Firefox](https://addons.mozilla.org/cs/firefox/addon/webfilter/) | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="48px" height="48px" /><br/>Chrome](https://chrome.google.com/webstore/detail/webfilter/cacopjlooplkklbefpaojbongcjaeoll) | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt="Edge" width="48px" height="48px" /><br/>Edge](https://microsoftedge.microsoft.com/addons/detail/webfilter/jojimgafjdoihlcllgkhmpjjkleacbjc) |
| --------- | --------- | --------- | --------- |

Rozšíření webových prohlížečů instalujte pouze z oficiálních obchodů.

## Funkce

### Filtrování webových stránek podle kategorií

Uživatel může zvolit až **7 kategorií webových stránek k blokování**:
* Sociální sítě (_Facebook, Instagram, Twitter, ..._)
* Zasílání zpráv a chaty (_Messenger, WhatsApp, Discord, ..._)
* Streamování videí (_YouTube, TikTok, Netflix, ..._)
* Online hry (_Steam, EpicGames, Miniclip, ..._)
* Stránky se smíšeným obsahem (_9gag, Reddit, Imgur, ..._)
* Seznamky (_Tinder, Badoo, OkCupid, ..._)
* Stránky s obsahem pro dospělé (_explicitní webové stránky_)

**WebFilter čte pouze URL adresy záložek prohlížeče a webových požadavků**. Doménová jména těchto URL adres jsou porovnávána s URL blacklistem složeným na základě vybraných kategorií. Kompletní blacklist obsahuje přibližně **400 doménových jmen**. Pokud je nalezena shoda, pak je stránka/požadavek zablokována a zobrazí se informační stránka.

### Volitelný časový zámek

Nastavení rozšíření WebFilter může být volitelně uzamknuto na vybraný časový interval.

### Snímek obrazovky s rozšířením
<img src="docs/chrome.cs.png" width="640px" />

## Licence

>**WebFilter je zdarma.**<br/>
>Všichni smí používat toto rozšíření zdarma.
>Mějte však na paměti náklady na jeho vývoj. A nezapomínejte ani na ušetřený čas bez prokrastinace díky tomuto rozšíření. Pokud se vám WebFilter líbí, prosím, [**podpořte**](https://www.buymeacoffee.com/pchcze) jeho vývoj tím, že koupíte autorovi kávu.

>**Zdrojový kód WebFilter je otevřený a veřejný.**<br/>
>Každý si může prohlédnout a zrevidovat zdrojový kód rozšíření WebFilter v tomto [**GitLab Repozitáři**](https://gitlab.com/Pecha/webfilter/-/tree/main). Tento software a jeho zdrojový kód jsou duševním vlastnictvím autora. Žádná část nesmí být dále šířena ani jakkoliv upravována bez výslovného souhlasu autora.

>**WebFilter nesbírá, neukládá ani nesdílí žádná data.**<br/>
>Soukromí uživatelů je pro WebFilter prioritou. Všechno se děje lokálně na počítači uživatele. Vlastní nastavení uživatelů je ukládáno v rámci webového prohlížeče.

## Podpora

Chtěli byste zablokovat další webovou stránku? Máte nápad na rozšíření? Našli jste chybu?

**Dejte o tom vědět založením nového [Gitlab Issue](https://gitlab.com/Pecha/webfilter/-/issues).**

##  Líbí se vám WebFilter?

Podpořte jeho vývoj nebo poděkujte autorovi.

<a href="https://www.buymeacoffee.com/pchcze" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-yellow.png" alt="Buy Me A Coffee" height="41" width="174"></a>
---

**&copy; Jan Pecha 2022**

Navštivte moje webové stránky [https://jan.pecha.it/](https://jan.pecha.it/)

