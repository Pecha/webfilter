# <img src="docs/icon32.png" /> WebFilter
<a href="#installation"><img src="https://shields.io/badge/-Browser%20extension-565656" /></a>
<a href="https://jan.pecha.it/" target="_blank"><img src="https://shields.io/badge/Author-Jan%20Pecha-157fbd" /></a>
<a href="https://gitlab.com/Pecha/webfilter/-/issues" target="_blank"><img src="https://shields.io/badge/GitLab-Issues-52c228" /></a>
<a href="https://www.buymeacoffee.com/pchcze" target="_blank"><img src="https://img.shields.io/badge/-☕%20Buy%20me%20a%20coffe-fbdb04" /></a>

**WebFilter allows users to block specific websites using categorical blacklist of URLs.**

_**Purpose: Boost your productivity, keep you from distractions and prevent procrastination.**_

Page language:

<a href="./README.md"><img src="https://raw.githubusercontent.com/madebybowtie/FlagKit/master/Assets/PNG/GB.png" /> **English**</a>
/ <a href="./README.CS.md"><img src="https://raw.githubusercontent.com/madebybowtie/FlagKit/master/Assets/PNG/CZ.png" /> Česky</a>

* [Installation](#installation)
* [Features](#features)
    * [Filtering websites by categories](#filtering-websites-by-categories)
    * [Optional time lock](#optional-time-lock)
* [License and disclaimer](#license-and-disclaimer)
* [Support](#support)
* [Do you like WebFilter?](#do-you-like-webfilter)

## Installation

| [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/opera/opera_48x48.png" alt="Opera" width="48px" height="48px" /><br/>Opera](https://addons.opera.com/en/extensions/details/webfilter/) | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_48x48.png" alt="Firefox" width="48px" height="48px" /><br/>Firefox](https://addons.mozilla.org/en-US/firefox/addon/webfilter/) | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_48x48.png" alt="Chrome" width="48px" height="48px" /><br/>Chrome](https://chrome.google.com/webstore/detail/webfilter/cacopjlooplkklbefpaojbongcjaeoll) | [<img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_48x48.png" alt="Edge" width="48px" height="48px" /><br/>Edge](https://microsoftedge.microsoft.com/addons/detail/webfilter/jojimgafjdoihlcllgkhmpjjkleacbjc) |
| --------- | --------- | --------- | --------- |

You should install browser extensions only from official stores.

## Features

### Filtering websites by categories

Users can select up to **7 categories of websites to be blocked**:
* Social networks (_Facebook, Instagram, Twitter, ..._)
* Messaging and chats (_Messenger, WhatsApp, Discord, ..._)
* Videos and streaming (_YouTube, TikTok, Netflix, ..._)
* Online games (_Steam, EpicGames, Miniclip, ..._)
* Mixed content (_9gag, Reddit, Imgur, ..._)
* Dating sites (_Tinder, Badoo, OkCupid, ..._)
* Adult content (_explicit websites_)

**WebFilter reads only URL arddresses of browser's tabs and web requests**. Domain names of these URL addresses are compared to custom URL blacklist based on selected categories. The complete blacklist contains about **400 domain names**. If the match is found, then the site/request is blocked and an info page is shown.

### Optional time lock

WebFilter settings can be optionally locked for selected time interval.

### Extension screenshot
<img src="docs/chrome.png" width="640px" />


## License and disclaimer

>**WebFilter is free to use.**<br/>
>Everyone can use this extension for free.
>However keep in mind costs of its development. And remember all the saved time without procrastination. If you like this extension, please [**support**](https://www.buymeacoffee.com/pchcze) its development by buying a coffe for the author.

>**WebFilter's source code is open and public.**<br/>
>Everyone can review the source code of WebFilter in this [**GitLab Repository**](https://gitlab.com/Pecha/webfilter/-/tree/main). This software and its source code is the intellectual property of the author. Any of its parts cannot be further distributed or modified in any way without express consent. 

>**WebFilter does not collect, store nor share any data.**<br/>
>User's privacy is WebFilter priority. All the magic happens locally on user's computer. User's custom settings are saved inside web browser.

## Support

Would you like to block another website? Any ideas for improvement? Found an error?

**Create new [Gitlab Issue](https://gitlab.com/Pecha/webfilter/-/issues).**

##  Do you like WebFilter?

Support its development or say thanks to author.

<a href="https://www.buymeacoffee.com/pchcze" target="_blank"><img src="https://cdn.buymeacoffee.com/buttons/default-yellow.png" alt="Buy Me A Coffee" height="41" width="174"></a>
---

**&copy; Jan Pecha 2022**

Visit my website [https://jan.pecha.it/](https://jan.pecha.it/)
