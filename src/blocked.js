(function () {
    // Apply translations
    WebFilter.translate();
    // Function random(min, max) to generate random int from given interval
    function random(min, max) { return Math.floor((Math.random() * ((max - min) + 1)) + min); }
    // Show random message about blocking website to user
    document.getElementById("message").innerHTML = WebFilter.R("m" + random(1, 5));
    // Show blocked URL
    document.getElementById("url").innerHTML = location.search.substring(1);
})();