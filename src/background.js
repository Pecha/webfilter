WebFilter.ready(function () {

    // #region Filter created requests
    WebFilter.Browser.webRequest.onBeforeRequest.addListener(function (request) {
        if (request.url != undefined && WebFilter.filter(request.url)) {
            if (request.type == "main_frame") {
                WebFilter.Browser.tabs.query({ 'active': true, 'lastFocusedWindow': true }, function (tabs) {
                    WebFilter.Browser.tabs.update({ url: "blocked.html?" + (new URL(request.url)).hostname });
                });
            }
            return { cancel: true };
        }
    }, {
        urls: ["*://*/*"],
        types: ["main_frame", "sub_frame", "image", "media", "xmlhttprequest", "script", "websocket", "other"]
    }, ["blocking"]);
    // #endregion

    // #region Filter all opened tabs
    function filterTabs() {
        WebFilter.Browser.tabs.query({}, function (tabs) {
            for (var i = 0; i < tabs.length; i++) {
                if (tabs[i].url != undefined && WebFilter.filter(tabs[i].url)) {
                    WebFilter.Browser.tabs.update(tabs[i].id, { url: "blocked.html?" + (new URL(tabs[i].url)).hostname });
                }
            }
        });
    }
    WebFilter.Browser.tabs.onUpdated.addListener(function () { WebFilter.ready(filterTabs); });
    WebFilter.Browser.storage.onChanged.addListener(function () { WebFilter.ready(filterTabs); });
    filterTabs();
    // #endregion
});
