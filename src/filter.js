/**
 * WebFilter.filter method
 * @param {String} url - tab or request url
 * @return {Boolean} - true = block, false = allow
 */
WebFilter.filter = function (url) {
    try {
        // Hostname from string URL
        var hostname = (new URL(url)).hostname;
        // Split hostname to parts
        var hostnameParts = hostname.split(".");
        // Second level domain name
        // Some sites use more than one first level domain name and many third level domain names; rather block more sites than less
        var domain = hostnameParts.length > 1 ? hostnameParts[hostnameParts.length - 2] : hostnameParts[0];

        // #region Domain blacklist
        // Blacklist is compiled from lists on Wikipedia.org, SimilarWeb.com, etc.
        var filter = "";

        // #region Social networks
        if (!WebFilter.Config.get("social")) {
            filter += "|facebook|instagram|twitter|vk|linkedin|weibo|qq|myspace";
        }
        // #endregion

        // #region Messaging and chats
        if (!WebFilter.Config.get("chats")) {
            filter += "|messenger|telegram|discord|snapchat|whatsapp|kik|omegle|skype|viber|wechat|disqus|yammer";
        }
        // #endregion

        // #region Videos and streaming
        if (!WebFilter.Config.get("videos")) {
            filter += "|youtube|vimeo|dailymotion|stream|tiktok|netflix|twitch|primevideo|hbomax|disneyplus|disney|hulu|kuaishou|tudou|dlive|rumble|rutube|younow|veoh|imdb|screenrant"
                + "|sledujserialy|najserialy|freefilm|kukaj|edna|tvguru|csfd|prehraj|bombuj|sledovanitv|filmtoro|sledujfilmy|joj|filmhub|filmyserialy|ocko|barandov|sledujteto|filmix"
                + "|spustit|nahnoji|flixtor|sosac|fmovies|mxplayer|miguvideo|cda|mediaset|sky|raiplay|filmweb|mgtv|afreecatv|clipwatching|streamtape|mixdrop|justwatch|tvspielfilm";
        }
        // #endregion

        // #region Online games
        if (!WebFilter.Config.get("games")) {
            filter += "|steampowered|steamcommunity|gog|ign|ubisoft|activision|blizzard|gameloft|epicgames|gamesradar|polygon|gamefront|gamerant|gamerevolution|wowhead|nga"
                + "|miniclip|addictinggames|agame|pogo|playretrogames|kongregate|mmogames|roblox|chess|gamewith|gamepedia|kahoot|gamespot|ea|nintendo|3dmgame|gamersky"
                + "|worldoftanks|forgeofempires|wargaming|divokekmeny|pathofexile|superhry|gameforge|zatrolene-hry|sfgame|travian|gametwist|worldofwarships|hrej|aternos"
                + "|vortex|playstation|sudokuonline|elvenar|minecraft|duelovky|humblebundle|worldofwarcraft|warcraftlogs|ctrlv|xzone|pcgamer|riotgames|warthunder|howrse"
                + "|railnation|xbox|dracik|gamesites|eurogamer|leagueoflegends|battle|guildwars|grepolis|mmo-champion|skribbl|majnr|prokonzole|rockstargames|webgames"
                + "|twinstar|warmane|tanks|eneba|champion";
        }
        // #endregion

        // #region Mixed content
        if (!WebFilter.Config.get("mixed")) {
            filter += "|9gag|imgur|reddit|pinterest|tumblr|deviantart|4chan|quora|boredpanda|ask|taringa|blogger|fotki|flickr|reactor|joyreactor|mix|ok|knowyourmeme"
                + "|loupak|tycico|1gr|super|blesk|refresher|fanfiction|fandom|grid|pagesix|ifunny|imgflip|adme|pikabu|fishki|thechive|funnyjunk|cracked|kwejk"
                + "|cheezburger|lelum|thewrap|imgsmail|anekdot|tc-link|webfail|notalwaysright|theonion|playbuzz|memedroid|somethingawful|mojevideo|uberhumor"
                + "|hugelol|ruinmyweek|";
        }
        // #endregion

        // #region Dating sites
        if (!WebFilter.Config.get("dating")) {
            filter += "|tinder|badoo|grindr|lovoo|okcupid|bumble|twoo|cupid|eharmony|match|adultfriendfinder";
        }
        // #endregion

        // #region Adult content
        if (!WebFilter.Config.get("adult")) {
            filter += "|pornhub|xvideos|xnxx|sex|xhamster|indexxx|coedcherry|pornpics|pichunter|bongacams|chaturbate|stripchat|spankbang|onlyfans|youporn|livejasmin"
                + "|redtube|daftsex|anybunny|youjizz|brazzersnetwork|txxx|beeg|nutaku|redwap|hqporner|sxyprn|sxypix|myfreecams|hdsex|tnaflix|tubesafari|eporner|pornhat"
                + "|perfectgirls|theponrdude|thenude|literotica|ymlporn|bangbros|glamour|joymii|morazzia|urlgalleries|sexart|metart|girlsreleased|imagetwist|candidium"
                + "|gonewild|forumophilia|cliphunter|wowgirls|sexvideo|fakehub|faketaxi|czechcasting|godsartnudes|freevideo|amateri|amaterky|realsrv|pvideo|dlouha-videa"
                + "|ixxx|pornlife|pornuj|xfantazy|noodlemagazine|porntrex|imagefap|xchat|upornia|anysex|redgifs|hellporno|letmejerk|fuq|jizzbunker|tubespin|modelhub"
                + "|motherless|myporno|luxuretv|camwhores|pornzog|hotmovs|rychlyprachy|planetsuzy|pervclips|fapster|fap|biguz|iwank|thumbzilla|maturetube|pornhd"
                + "|pervers|mature|xbabe|3movs|babestube|porn|playvids|xxxfiles|bravoporn|sexvid|pornolab|aznude|peekvids|viptube|nahefoto|xtube|xcafe|freeones";
            filter += "|bet365|bet9ja|tipsport|sazka|ifortuna|synottip|isoftbet|registeramo|sportradar|20tracks|myfortuna|betarena|bets|eurojackpot|kimvegas|flashscore"
                + "|pokerarena|onecasino|oddsportal|goal|milionar|777score|tipos|casinoarena|22bet|pokerzive|woocasino|sportkadnes|loterie-tikety|tsars|zyngapoker"
                + "|flalottery|lotterypost|bovada|casino|skybet|skybetservices|williamhill|platincasino|interwetten|stoloto|fonbet|europacasino|bs|bitcasino|twin"
                + "|dailywinners|tai789|roobet|888casino|virgingames|betano|jackpotoffers24|bestgamesvault|privatewinners|kingbillycasino|leoveges|betmgm|casinoroom"
                + "|vegasworld|coolbet|intercasino|goxbet2|dailydealsreview|casinoworld|32red|hollywoodbets|txlottery|lotto|galottery|powerball|nclottery|lotteryusa";
        }
        // #endregion

        filter += "|";
        // #endregion

        // Return boolean indicating whether the site should be blocked
        return filter.indexOf("|" + domain + "|") != - 1;
    } catch (e) {
        // In case of exception - do not block
        return false;
    }
};