var WebFilter = (function () {

    var browser = chrome;

    // Default config
    var config = {
        social: true,
        chats: true,
        videos: true,
        games: false,
        mixed: false,
        dating: false,
        adult: false,
        locked: false
    }

    /**
     * Ready - wait for config to be loaded from browser storage
     * @param {Function} fn callback
     */
    function ready(fn) {
        browser.storage.local.get(["config"], function (result) {
            if (result != undefined && result.config != undefined) {
                config = result.config;
                if (config.locked !== false && (config.locked * 1) < ((new Date()) * 1)) {
                    store("locked", false)
                }
            }
            if (fn != undefined) {
                fn();
            }
        });
    }

    /**
     * Store - get/set config item
     * @param {any} k key
     * @param {any} v value
     */
    function store(k, v) {
        // If new value -> update config and storage
        if (v != undefined) {
            config[k] = v;
            browser.storage.local.set({ "config": config });
        }
        // Return value
        return config[k];
    }

    // Apply i18n translations on document
    function translate() {
        var elements = document.querySelectorAll('[data-translate]');
        for (var i = 0; i < elements.length; i++) {
            elements[i].textContent = WebFilter.R(elements[i].dataset.translate);
        }
    }

    // Public interface
    return {
        Config: {
            get: function (k) { return store(k) },
            set: function (k, v) { store(k, v); },
        },
        // ready - wait for WebFilter config to load
        ready: ready,
        // translate - apply translates
        translate: translate,
        // Browser - alias for chrome object
        Browser: browser,
        // R - return resource (text translation)
        R: function (key) { return browser.i18n.getMessage(key); }
    };
})();