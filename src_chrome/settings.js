// Apply translations
WebFilter.translate();

// When filter is ready
WebFilter.ready(function () {

    // Category keys
    var keys = ["social", "videos", "games", "chats", "mixed", "dating", "adult"];

    // #region Time lock
    function showLockedInfo(until) {
        // Hide time lock menu
        document.getElementById("time-lock").style.display = "none";
        // Show info about remaing lock time
        document.getElementById("locked-info").innerHTML = "<div class='row'>" + WebFilter.R("lockedUntil") + " " + (new Date(until * 1)).toLocaleTimeString() + ". " + WebFilter.R("comeBackLater") + "</div>";
    }
    var locked = WebFilter.Config.get("locked");
    if (locked !== false) {
        showLockedInfo(locked);
    }
    // Lock button click
    document.getElementById("lock").addEventListener("click", function () {
        // Add selected time interval
        var until = ((new Date(((new Date()) * 1) + (document.getElementById("time").value * (60 * 1000)))) * 1);
        // Save lock time
        WebFilter.Config.set("locked", until);
        // Show locked info
        showLockedInfo(until);
        // Disable all inputs
        for (var i = 0; i < keys.length; i++) {
            document.getElementById(keys[i]).disabled = true;
        }
    });
    // #endregion

    // #region Settings init
    for (var i = 0; i < keys.length; i++) {
        (function (key) {
            var input = document.getElementById(key);
            // Load saved / default value
            var value = WebFilter.Config.get(key);
            input.type == "checkbox" || input.type == "radio" ? input.checked = (value == true || value == "true") : input.value = value;
            // Disable if time lock is on
            if (locked === false) {
                input.disabled = false;
            }
            // Event listener to update saved value
            function update(event) {
                WebFilter.Config.set(key, input.type == "checkbox" || input.type == "radio" ? input.checked : input.value);
            }
            // Attaching listeners
            input.addEventListener("change", update);
            input.addEventListener("click", update);
        })(keys[i]);
    }
    // #endregion

});