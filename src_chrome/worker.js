importScripts('core.js');
importScripts('filter.js');  

WebFilter.ready(function (initConfig) {
    function filterTabs() {
        WebFilter.Browser.tabs.query({}, function (tabs) {
            for (var i = 0; i < tabs.length; i++) {
                if (tabs[i].url != undefined && WebFilter.filter(tabs[i].url)) {
                    WebFilter.Browser.tabs.update(tabs[i].id, { url: "blocked.html?" + (new URL(tabs[i].url)).hostname });
                }
            }
        });
    }
    WebFilter.Browser.tabs.onUpdated.addListener(function () { WebFilter.ready(filterTabs); });
    WebFilter.Browser.storage.onChanged.addListener(function () { WebFilter.ready(filterTabs); });
    filterTabs(initConfig);
});
